var _mobileEventCallback = null;


/*
 이벤트 진행
 event_code : 이벤트 코드
 yn0, yn1 : 'Y'/'N' 값 2개
 arrParam : 50바이트의 입력값 6개(6개까지 입력가능), 600바이트(1개)
*/
function mobile_event(event_code, yn1, yn2, arrParam, callback)
{
    _mobileEventCallback = callback;


	var header = "hwsimple://events?";
	var msg = "";
    
    var list = {
        "event_code" : event_code,
        "yn1" : yn1,
        "yn2" : yn2,
        "params" : arrParam
    };
    var msg = JSON.stringify(list);
    window.location = header + msg;
}

/*
 설문조사 진행
 survey_code : 설문조사 아이디
 result : 설문조사 결과 (sid, value의 배열)
   - sid : 설문조사항목 아이디
   - value : 설문조사 항목 (600바이트)
result =
[
{
	"sid" : "0001",
	"value" : "600바이트까지 입력가능합니다."
},
{
	"sid" : "0002",
	"value" : "무한개로 입력이 가능합니다."
},
{
	"sid" : "0003",
	"value" : "항목을 입력하세요."
}
]
*/
function mobile_survey(survey_code, result){
	var header = "hwsimple://survey?";
    var list = {
        "event_code" : survey_code,
        "list" : result
    };
    
	var msg = JSON.stringify(list);
    window.location = header + msg;
}

/*
 전체화면 열기(팝업 -> 풀화면)
 url : 해당URL을 전체화면으로 열기
 */
function mobile_open_fullscreen(url){
    console.log('[mobile_open_fullscreen]', url);

    var header = "hwsimple://open_full?";
    var list = {
        "url" : url
    };
    var msg = JSON.stringify(list);
    window.location = header + msg;
}

/*
 전체화면 닫기
 */
function mobile_close_fullscreen(url){
    var header = "hwsimple://close_full?";
    var list = {
        "url" : url
    };
    
    var msg = JSON.stringify(list);
    window.location = header + msg;
}

/*
 이벤트 종료, 7일동안보지않기 처리
 nomore_show_second : n(양수 - n일 동안 다시보지 않기), 0(계속보기), -1(다시보지 않기 무제한)
 */
function mobile_close_event(nomore_show){
    var header = "hwsimple://closed_event?";
    var list = {
        "nomore_time" : nomore_show
    };
    
    var msg = JSON.stringify(list);
    window.location = header + msg;
}

/*
 서버에서 이벤트를 전송후 받은 응답값처리
 msg : 서버응답값
 */
function mobile_response(data){
    // 모바일에서 Callback으로 호출되며, 이부분을 Custom하시면 됩니다.
    //alert(data);
    if( typeof(_mobileEventCallback) == 'function' ){
        _mobileEventCallback.call(this, data);
    }

    _mobileEventCallback = null;
}

/*
  모바일 커스텀 팝업
  title : 팝업 타이틀
  msg : 팝업 메시지
  ret : return type
 */
function mobile_alert(title, msg){
    var header = "hwsimple://event_alert?";
    var list = {
        "title" : title,
        "msg" : msg
    };
    
    var msg = JSON.stringify(list);
    window.location = header + msg;
}

function mobile_version_check(){
    var req = new XMLHttpRequest();
    req.open('HEAD', document.location, true);
    req.send(null);
    var headers = req.getAllResponseHeaders().toLowerCase();
    
    var app_ver = headers["app_ver"];
    var os_ver = headers["os_ver"];
    var os_ = headers["os"];

    //alert(app_ver + " " + os_ver + " " + os_);

}


/**
* 닫기버튼
*/
function closePage(){
    mobile_close_event(0);
}

/**
* 오늘 하루 보지 않음
*/
function closeToday(){
    mobile_close_event(1);
}


/**
* 전체화면으로 랜딩된 페이지 닫기
*/
function closeFullscreen( linkUrl ){
    mobile_close_fullscreen( (linkUrl || window.location.href) );
}


/**
* 시스템 안내 메시지 레이어 팝업 표시
*/
function showMessageLayer( val ){
	var $layer = $('.layer, .system-alert' + val);
	$layer.fadeIn();
}



$(document).ready( function() {
	$('.layer a').on('click', function(e){
		e.preventDefault();
		$('.layer, .system-alert').fadeOut();
	});
});